<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Redirect;
use Carbon\Carbon;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        // $this->middleware('auth.basic');
       /* $this->middleware('TokenAuthMiddleware');*/
    }

    
    /**
     * Show the application dashboard.
     * @Middleware("auth.basic")
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->getData();
        return view('index', compact('data', $data));
    }
    public function edit($id)
    {
        $data = $this->getData();
        foreach($data as $item)
        {
            if($item->id == $id)
            {
                return view('product', compact('item', $item));
            }
        }
        return view('index', compact('data', $newdata));
    }
    public function editproduct(Request $request)
    { 
		
        $input = $request->all();
        $data = $this->getData();
        $newdata = array();
        foreach($data as $item)
        {
            if($item->id == $input['id'])
            {
                $item->name = $input["name"];
                $qtt = $input["quantity"];
                $item->quantity = $qtt;
                $prc = $input['price'];
                $item->price = $prc;
                $item->submitted = $input["submitted"];
                $tol = (float)$prc * (int)$qtt;
                $item->total = (float)$tol;
            }
            array_push($newdata, $item);
        }
        $this->setData(json_encode($newdata));
        return view('index', compact('data', $newdata));
    }
    public function delete($id)
    {
        $data = $this->getData();
        $newdata = array();
        foreach($data as $item)
        {
            if($item->id != $id)
            {
                array_push($newdata, $item);
            }
        }
        $this->setData(json_encode($newdata));
        return Redirect::back()->with('message','Delete Operation Successful !');
    }

    public function createitem(){
        return view('createitem');
    }
    public function product(Request $request){ 
		
        $input = $request->all();
        if($input['name']){
            $data = $this->getData();
            $product = (object) array();
            $product->id = count($data);
            $product->name = $input["name"];
            $qtt = $input["quantity"];
            $product->quantity = $qtt;
            $prc = $input['price'];
            $product->price = $prc;
            $item->submitted = Carbon::now();
            $tol = (float)$prc * (int)$qtt;
            $item->total = (float)$tol;
            array_push($data, $product);

            $this->setData(json_encode($data));
            return view('index', compact('data', $data));
        }
    }

    protected function getData(){
        $path = storage_path() . "/json/datafile.json";
        
        $data = json_decode(file_get_contents($path));
        if(empty($data) || !is_array($data)){
            $data = $this->getDefaultData();
        }
        return $data;
    }
    protected function setData($jsonstring){
        $path = storage_path() . "/json/datafile.json";
        file_put_contents($path, $jsonstring);
    }
    protected function getDefaultData(){
        return json_decode('[{"id":0,"name":"aplly","quantity":5,"price":2.05,"submitted":"2016-01-09","total":10.10},
                {"id":1,"name":"orange","quantity":50,"price":1.00,"submitted":"2016-01-09","total":50.00},
                {"id":2,"name":"pear","quantity":15,"price":1.20,"submitted":"2016-01-09","total":18.00},
                {"id":3,"name":"peach","quantity":5,"price":1.50,"submitted":"2016-01-09","total":7.50},
        {"id":4,"name":"banana","quantity":5,"price":0.50,"submitted":"2016-01-09","total":2.50}]');
    }
}
