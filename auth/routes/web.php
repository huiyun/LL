<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

ini_set("allow_url_fopen", 1);
Route::get('/jsonfile', function () {
    $path = storage_path() . "/json/datafile.json";
    $json = file_get_contents($path);
    return $json;
});
Route::get('/index', 'ProductController@index');
Route::get('createitem', 'ProductController@createitem');
Route::post('createitem', 'ProductController@product');

Route::get('edit/{id}', 'ProductController@edit');
Route::post('edit', 'ProductController@editproduct');
Route::get('delete/{id}', 'ProductController@delete');
Route::post('product', 'ProductController@product');