@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
	<h1> Laravel Create a Product </h1>
	<hr/>
 <form class="col-md-8 form-group" role="form" method="POST" action="{{ url('/product') }}">
           <div class="content">
           {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Product Name</label>

                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control" name="name" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="quantity" class="col-md-4 control-label">Quantity</label>

                            <div class="col-md-8">
                                <input id="quantity" type="text" class="form-control" name="quantity" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-md-4 control-label">Price</label>

                            <div class="col-md-8">
                                <input id="price" type="text" class="form-control" name="price" value="">
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label for="submitted" class="col-md-4 control-label">Date time submitted</label>

                            <div class="col-md-8">
                                <input id="submitted" type="text" class="form-control" name="submitted" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="total" class="col-md-4 control-label">Total</label>

                            <div class="col-md-8">
                                <input id="total" type="text" class="form-control" name="total" value="">
                            </div>
                        </div>-->
                      <div class="form-group">
                      <div class="col-md-6 col-md-offset-4">
                        <button id="product_btn" type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn"></i> Create a Product
                        </button>
                      </div></div>    
            </div>
</form>
</div></div>	
@stop
