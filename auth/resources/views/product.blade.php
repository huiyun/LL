@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
	<h1> Laravel Create a Product </h1>
	<hr/>
 <form class="col-md-8 form-group" role="form" method="POST" action="{{ url('/edit') }}">
           <div class="content">
           {{ csrf_field() }}
           @if(!is_null($item)) 
           <input type="hidden" name="id" value="{{$item->id}}">
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Product Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{$item->name}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="quantity" class="col-md-4 control-label">Quantity</label>

                            <div class="col-md-6">
                                <input id="quantity" type="text" class="form-control" name="quantity" value="{{$item->quantity}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-md-4 control-label">Price</label>

                            <div class="col-md-6">
                                <input id="price" type="text" class="form-control" name="price" value="{{$item->price}}">
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="submitted" class="col-md-4 control-label">Date time submitted</label>

                            <div class="col-md-6">
                                <input id="submitted" type="text" class="form-control" name="submitted" value="{{$item->submitted}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="total" class="col-md-4 control-label">Total</label>

                            <div class="col-md-6">
                                <input id="total" type="text" class="form-control" name="total" value="{{$item->total}}">
                            </div>
                        </div>
                    
                        <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button id="product_btn" type="submit" class="btn btn-primary">
                                <i class="fa fa-btn"></i> Edit a Product
                           </button>
                        </div></div>    
       @endif     </div>
</form>

</div></div>	
@stop
