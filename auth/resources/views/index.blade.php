@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
	<h1> List Product Items: </h1>
	<hr/>	 
<form>
	<div class="class="col-md-8 form-group">
    <table class="col-md-8"><tbody><tr><th type="hiden"></th><th>Product Item Name</th><th>Quantity</th>
    <th>Price</th><th>Datetime submitted</th><th>Total value</th><th>Action</th></tr>
              @if(!is_null($data)) 
                    @foreach ($data as $values)
                        <tr id="{{$values->id}}"><td class="id"><div style="visibility: hidden; display:inline;">{{$values->id}}</div></td>
                            <td class="name"><h3>{{$values->name}}</h3></td>
                            <td class="quantity">{{$values->quantity}}</td>
                            <td class="price">{{$values->price}}</td>
                            <td class="submitted">{{$values->submitted}}</td>
                            <td class="total">{{$values->total}}</td>
                            <td><a align="center" href="{{ action('ProductController@edit',[$values->id])}}"> Edit </a>&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;<a align="center" href="{{ action('ProductController@delete',[$values->id])}}"> Delete </a></td></tr>
                    @endforeach
                @else
                    <tr><td><div style="visibility: hidden; display:inline;"></div></td>
                    <td><h3>No Record existing</h3></td><td></td><td></td></tr>
                @endif
                   </tbody> </table>
<div class="col-md-8 form-group"><a align="center" href="{{ action('ProductController@createitem')}}"> Create a New Product Item</a></div>
    </form>

</div></div>
<!--<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
function(id){
    var $cells = $("#table tr").eq(id).find("td");
    var values = {};
    $cells.each(function()
    {   var class = $(this).attr('class');
        if(class){
            values[class] = $(this).html();
        }    
    });
    return values;
}
</script>-->
@stop