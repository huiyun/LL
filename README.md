## LARAVEL tiny PROJECT 

- install MAMP: https://www.mamp.info/en/downloads/

- install composer: https://getcomposer.org.

- install laravel run below line code in terminal: (you could change the version)

composer global require "laravel/installer=~1.1"

## You must have OpenSSL 1.02 above version to be able to run(or better security) for your project.

- MAMP Apache 2 has openssl 0.9.8zd doesn't work with tls 1.2:

https://gist.github.com/jfloff/5138826